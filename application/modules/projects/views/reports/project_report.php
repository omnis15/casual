<section class="panel">
	<header class="panel-heading">
		<h5 class="text-weight-semibold"><?php echo $title;?></h5>
		<a href="<?php echo site_url();?>reports/projects" class="btn btn-success btn-sm pull-right" style="margin-top:-30px;"><i class="fa fa-arrow-left"></i> Back to Report Dashboard</a>
	</header>
	<div class="panel-body">
	<?php echo $this->load->view('projects/reports/project_report_header','',true);?>
	</div>
</section>
<section class="panel">
	<div class="panel-body">
	
	
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
				

				<?php
				$meeting_type_where = 'project_id = '.$project_id;
				$meeting_type_table = 'meeting';
				$meeting_type_select = '*';
				$item_select  = $this->projects_model->get_project_content($meeting_type_table, $meeting_type_where,$meeting_type_select);
				$parameters2 = '';
				if($item_select->num_rows() > 0)
				{
					
					foreach ($item_select->result() as $key ) {
						# code...
						$meeting_id = $key->meeting_id;
						$meeting_type_id = $key->meeting_type_id;
						if($meeting_type_id == 1)
						{
							$activity_title = 'CEE';
						}
						else if($meeting_type_id == 2)
						{
							$activity_title = 'Stakeholders';
						}
						else
						{
							$activity_title = $key->activity_title;
						}

						$meeting_gender_where = 'attendees.attendee_id = meeting_attendees.attendee_id AND meeting_attendees.meeting_id  = '.$meeting_id;
						$meeting_gender_table = 'attendees,meeting_attendees';
						// $meeting_gender_select = '*';
						$total_pop  = $this->users_model->count_items_where($meeting_gender_table, $meeting_gender_where);

						$parameters2 .= "['".$activity_title."',".$total_pop."],";

					}
				}
				

				?>
			    <script type="text/javascript">
			      google.charts.load('current', {'packages':['corechart']});
			      google.charts.setOnLoadCallback(drawChart);
			      function drawChart() {

			        var data = google.visualization.arrayToDataTable([
			          ['MEETING TYPE', 'TOTAL POPULATION'],
			          <?php echo $parameters2;?>
			        ]);

			        var options = {
			          title: 'MEETING TYPE AND POPULATION ',
			          is3D: true,

			        };

			        var chart = new google.visualization.PieChart(document.getElementById('meeting_type_chart'));

			        chart.draw(data, options);
			      }
			    </script>
			    <div id="meeting_type_chart" style="width: 100%;"></div>
			    <div class="pull-right"><a href="#"></a></div>
			</div>
			<div class="col-md-6">
				<?php
			
					$parameters3 = '';
					//  get population per the meetings
					$meeting_where = 'attendees.attendee_id = meeting_attendees.attendee_id AND attendees.gender_id = 1 AND meeting_attendees.meeting_id  = meeting.meeting_id AND meeting.project_id = '.$project_id;
					$meeting_table = 'attendees,meeting_attendees,meeting';
					$male_pop  = $this->users_model->count_items_where($meeting_table, $meeting_where);


					$meeting_gender_where = 'attendees.attendee_id = meeting_attendees.attendee_id AND attendees.gender_id = 2  AND meeting_attendees.meeting_id = meeting.meeting_id AND meeting.project_id = '.$project_id;
					$meeting_gender_table = 'attendees,meeting_attendees,meeting';
					// $meeting_gender_select = '*';
					$total_pop  = $this->users_model->count_items_where($meeting_gender_table, $meeting_gender_where);

					$parameters3 .= "['MALE',$male_pop],";
					$parameters3 .= "['FEMALE',$total_pop],";


				

				?>
				<script type="text/javascript">

			      google.charts.setOnLoadCallback(drawChart2);
			      function drawChart2() {

			        var data2 = google.visualization.arrayToDataTable([
			          ['GENDER', 'POPULATION'],
			          <?php echo $parameters3;?>
			        ]);

			        var options2 = {
			          title: 'CUMMULATIVE RATIO OF MALE TO FEMALE ',
			          is3D: true,

			        };

			        var chart2 = new google.visualization.PieChart(document.getElementById('piechart_div'));

			        chart2.draw(data2, options2);
			      }
			    </script>
			    <div id="piechart_div" style="width: 100%;"></div>
			    <div class="pull-right"><a href="<?php echo base_url();?>meeting-report/<?php echo $project_id;?>">explore</a></div>
			</div>
			
		</div>	
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
				<?php
			
					$parameters4 = '';
					//  get population per the meetings
					$community_where = 'community_group_member.community_group_id = community_group.community_group_id AND community_group_member.gender_id = 1 AND community_group.project_id = '.$project_id;
					$community_table = 'community_group_member,community_group';
					$com_male_pop  = $this->users_model->count_items_where($community_table, $community_where);


					$community_where = 'community_group_member.community_group_id = community_group.community_group_id AND community_group_member.gender_id = 2 AND community_group.project_id = '.$project_id;
					$community_table = 'community_group_member,community_group';
					// $community_gender_select = '*';
					$com_total_pop  = $this->users_model->count_items_where($community_table, $community_where);

					$parameters4 .= "['MALE',".$com_male_pop."],";
					$parameters4 .= "['FEMALE',".$com_total_pop."],";


				// var_dump($parameters4); die();

				?>
				<script type="text/javascript">
			      google.charts.setOnLoadCallback(drawChart2);
			      function drawChart2() {

			        var data2 = google.visualization.arrayToDataTable([
			           ['GENDER', 'POPULATION'],
			         	<?php echo $parameters4;?>
			        ]);

			        var options2 = {
			          title: 'CUMMULATIVE COMMUNITIES MALE TO FEMALE RATIO ',
			          is3D: true,

			        };

			        var chart2 = new google.visualization.PieChart(document.getElementById('piechart_div_add'));

			        chart2.draw(data2, options2);
			      }
			    </script>
			    <div id="piechart_div_add" style="width: 100%;"></div>
			    <div class="pull-right"><a href="<?php echo base_url();?>meeting-report/<?php echo $project_id;?>">explore</a></div>
			</div>
			<div class="col-md-6">
				<?php
				$seedling_status_where = 'seedling_status_id > 0 ';
				$seedling_status_table = 'seedling_status';
				$seedling_status_select = '*';
				$status_select  = $this->projects_model->get_project_content($seedling_status_table, $seedling_status_where,$seedling_status_select);
				$parameters5 = '';
				if($status_select->num_rows() > 0)
				{
					
					foreach ($status_select->result() as $select_key ) {
						$seedling_status_id = $select_key->seedling_status_id;
						$seedling_status_name = $select_key->seedling_status_name;

						$status_where = 'seedling_production.seedling_production_id = nursery_tally.seedling_production_id AND seedling_production.project_id  = '.$project_id.' AND nursery_tally.seedling_status_id = '.$seedling_status_id;
						$status_table = 'seedling_production,nursery_tally';
						$status_select = 'SUM(nursery_tally.quantity) AS number';
						$total_items  = $this->users_model->count_items_where($status_table, $status_where,$status_select);
						$parameters5 .= "['".$seedling_status_name."', ".$total_items."],";
					}
				}
				// var_dump($parameters5); die();
				?>
				<script type="text/javascript">
			      google.charts.setOnLoadCallback(drawChart);
			      function drawChart() {
			        var data = google.visualization.arrayToDataTable([
			          ['Seedling Status Name', 'No. of trees'],
			         <?php echo $parameters5;?>
			        ]);

			        var options = {
			          title: 'SEEDLINGS TALLY',
			          pieHole: 0.4,
			          
			        };

			        var chart = new google.visualization.PieChart(document.getElementById('seedling_tally_sheet'));
			        chart.draw(data, options);
			      }
			    </script>

			    <div id="seedling_tally_sheet" style="width: 100%;"></div>
			    <div class="pull-right"><a href="<?php echo base_url();?>meeting-report/<?php echo $project_id;?>">explore</a></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-12">
			<?php
				$followup_where = 'project_id = '.$project_id;
				$followup_table = 'planting_followup';
				$followup_select = '*';
				$followup_item  = $this->projects_model->get_project_content($followup_table, $followup_where,$followup_select);
				$parameters6 = '';
				if($followup_item->num_rows() > 0)
				{
					
					foreach ($followup_item->result() as $followup_key ) {
						# code...
						$planted_trees = $followup_key->planted_trees;
						$surviving_trees = $followup_key->surviving_trees;
						$year = $followup_key->year;
						$month = $followup_key->month;
						$parameters6 .= "['".$year."-".$month."', ".$planted_trees.", ".$surviving_trees."],";
					}
				}
			?>
			<script type="text/javascript">
			      google.charts.setOnLoadCallback(drawChart);
			      function drawChart() {
			        var data = google.visualization.arrayToDataTable([
			          ['Follow Up date', 'Planted Trees', 'Surviving Trees'],
			          <?php echo $parameters6;?>
			        ]);

			        var options = {
			          title: 'Planting Performance',
			          hAxis: {title: 'Follow-Up',  titleTextStyle: {color: '#333'}},
			          vAxis: {minValue: 0}
			        };

			        var chart = new google.visualization.AreaChart(document.getElementById('planting_performance'));
			        chart.draw(data, options);
			      }
			    </script>
			    <div id="planting_performance" style="width: 100%;height: 400px;"></div>

			</div>
		</div>
	</div>
	</div>
	
</section>