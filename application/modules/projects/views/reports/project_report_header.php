<?php
$seedling_where = 'project_watershed.project_area_id = project_areas.project_area_id  AND project_watershed.project_id  = '.$project_id;
$seedling_table = 'project_areas,project_watershed';
$seedling_select = 'COUNT(project_watershed.project_watershed_id) AS number';
$total_watersheds =  $this->users_model->count_items_where($seedling_table, $seedling_where,$seedling_select);


$meeting_where = 'meeting.project_id  = '.$project_id;
$meeting_table = 'meeting';

// count target areas
$totol_meetings = $this->users_model->count_items($meeting_table, $meeting_where);


$community_where = 'project_id  = '.$project_id;
$community_table = 'community_group';
$totol_communities = $this->users_model->count_items($community_table, $community_where);


$planting_site_where = 'project_id  = '.$project_id;
$planting_site_table = 'planting_site';
$total_planting_sites = $this->users_model->count_items($planting_site_table, $planting_site_where);




$seedling_where = 'order_receivables.project_id  = '.$project_id;
$seedling_table = 'order_receivables';
$seedling_select = 'SUM(order_receivables.quantity_given) AS number';
$total_seedlings =  $this->users_model->count_items_where($seedling_table, $seedling_where,$seedling_select);


$cpm_where = 'casual_payment.cp_project_id  = '.$project_id;
$cpm_table = 'casual_payment,casual_payment_members';
$cpm_select = 'SUM(casual_payment_members.cpm_id) AS number';
$cpm_group = 'casual_payment_members.cpm_national_id';
$total_cpms =  $this->users_model->count_items_group($cpm_table, $cpm_where,$cpm_select,$cpm_group);

$followup_where = 'planting_followup.project_id  = '.$project_id;
$followup_table = 'planting_followup';
$followup_select = 'planting_followup.surviving_trees AS number';
$total_surviving =  $this->users_model->count_items_group($followup_table, $followup_where,$followup_select);

$planted_where = 'planting_followup.project_id  = '.$project_id;
$planted_table = 'planting_followup';
$planted_select = 'planting_followup.planted_trees AS number';
$total_planted =  $this->users_model->count_items_group($planted_table, $planted_where,$planted_select);
if($total_planted == 0)
{
	$total_planted = 1;
}
if($total_surviving == 0)
{
	$total_surviving = 0;
}
$percentage = ($total_surviving/$total_planted)*100;

?>

<div class="panel-body center-align">
	<div class="col-md-2">
		<div class="h4 text-weight-bold mb-none"><?php echo $total_watersheds;?></div>
		<p class="text-xs text-muted mb-none">Watersheds</p>
	</div>
	<div class="col-md-2">
		<div class="h4 text-weight-bold mb-none"><?php echo $totol_meetings;?></div>
		<p class="text-xs text-muted mb-none"> Meetings</p>
	</div>
	<div class="col-md-2">
		<div class="h4 text-weight-bold mb-none"><?php echo $totol_communities;?></div>
		<p class="text-xs text-muted mb-none">Community Groups</p>
	</div>
	<div class="col-md-2">
		<div class="h4 text-weight-bold mb-none"><?php echo $total_planting_sites;?></div>
		<p class="text-xs text-muted mb-none">Planting Sites</p>
	</div>
	<div class="col-md-2">
		<div class="h4 text-weight-bold mb-none"><?php echo $total_seedlings;?></div>
		<p class="text-xs text-muted mb-none">Seedlings at CTN</p>
	</div>
	<div class="col-md-2 success">
		<div class="h4 text-weight-bold mb-none"><?php echo $percentage;?>%</div>
		<p class="text-xs text-muted mb-none"> Success Rate</p>
	</div>
</div>
