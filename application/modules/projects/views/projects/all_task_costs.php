<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Role Name</th>
						<th>Estimated Budget</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Estimated Days</th>
						<th>Operation</th>
						<th>Created</th>
						<th>Status</th>
						<th colspan="1">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				
				$task_cost_id = $row->task_cost_id;
				$task_cost_name = $row->task_cost_name;
				$start_date = $row->start_date;
				$end_date = $row->end_date;
				$operation_number = $row->operation_number;

				$startDate = strtotime($start_date);
				$endDate = strtotime($end_date);

				$interval = $endDate - $startDate;
				$task_time = 1 + floor($interval / (60 * 60 * 24));



				$casual_number = $row->casual_number;
				$task_casual_cost = $row->task_casual_cost;
				$operation_id = $row->operation_id;
				$operation_type_id = $row->operation_type_id;
			
				$task_budget = $casual_number*$task_casual_cost*$operation_number*$task_time;
				
				
				
				$created_by = $row->created_by;
				$created = $row->created;
				$project_status = $row->task_cost_status;
				$operation_name = $row->operation_name;

				
				
				//status
				if($project_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				//status
				
				//create deactivated status display
				if($project_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'projects/activate-task/'.$task_cost_id.'" onclick="return confirm(\'Do you want to activate '.$task_cost_name.'?\');" title="Activate '.$task_cost_name.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
					$button_tasks='';

				
				}
				//create activated status display
				else if($project_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-danger" href="'.site_url().'projects/project-tasks-costs-delete/'.$task_cost_id.'" onclick="return confirm(\'Do you want to Delete '.$task_cost_name.'?\');" title="Deactivate '.$task_cost_name.'"></i>Delete</a>';
					$button_tasks = '<a class="btn btn-warning" href="'.site_url().'projects/project-tasks-costs-delete/'.$task_cost_id.'" title=" Casual '.$task_cost_name.'"><i class="fa fa-user"></i> Casual Roles</a>';
			
				
				}
				
				
				
				
				$count++;
				$result .= 
				'
				

					<tr>
						<td>'.$count.'</td>
						<td>'.$task_cost_name.'</td>
						<td>'.$task_budget.'</td>
						<td>'.$start_date.'</td>
						<td>'.$end_date.'</td>
						<td>'.$task_time.'</td>
						<td>'.$operation_name.'</td>
						<td>'.$created.'</td>
						<td>'.$status.'</td>			
						<td>'.$button.'</td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no Roles";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				<div class="row" style="margin-bottom:20px;">
                                    <div class="col-lg-2 col-lg-offset-8">
                                        <a href="<?php echo site_url();?>projects/project-tasks-costs-add/<?php echo $project_id?>/<?php echo $task_id?>" class="btn btn-sm btn-info pull-right">Add Tasks Roles</a>
                                     </div>
                                     <div class="col-lg-2">
                                        <a href="<?php echo site_url();?>projects" class="btn btn-sm btn-info pull-right">Back to Projects</a>
                                     </div>
                                   
                                </div>

				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

