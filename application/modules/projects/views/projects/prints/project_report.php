<?php

$project_rs = $this->projects_model->get_project_detail($project_id);

if($project_rs->num_rows() > 0)
{
	$row = $project_rs->result();
	$project_id = $row[0]->project_id;
	$project_number = $row[0]->project_number;
	$project_title = $row[0]->project_title;
	$project_location = $row[0]->project_location;
	$project_grant_value = $row[0]->project_grant_value;
	$project_start_date = $row[0]->project_start_date;
	$project_end_date = $row[0]->project_end_date;

}






$area_where = 'project_areas.project_area_status = 1 AND project_areas.project_area_id = project_watershed.project_area_id AND project_watershed.project_id = '.$project_id;
$area_table = 'project_areas,project_watershed';

// count target areas
$total_project_areas = $this->users_model->count_items($area_table, $area_where);



$community_where = 'community_group_id > 0 AND project_id ='.$project_id;
$community_table = 'community_group';

// count target areas
$totol_communities = $this->users_model->count_items($community_table, $community_where);


$community_where = 'casual_gender = 1 AND casual_status = 1 AND casual.project_id ='.$project_id;
$community_table = 'casual';

// count target areas
$total_communities_member_male = $this->users_model->count_items($community_table, $community_where);



$community_where = 'casual_gender = 2 AND casual_status = 1  AND casual.project_id ='.$project_id;
$community_table = 'casual';

// count target areas
$total_communities_member_female = $this->users_model->count_items($community_table, $community_where);

$task_where = 'task_status = 1 AND project_id ='.$project_id;
$task_table = 'task';

// count target areas
$total_task = $this->users_model->count_items($task_table, $task_where);



$seedling_where = 'task_costs.task_id = task.task_id AND task.project_id = '.$project_id;
$seedling_table = 'task,task_costs';
$seedling_select = 'SUM(task_costs.task_casual_cost * task_costs.task_time * task_costs.operation_number) AS number';
$total_budgeted_mount  =  $this->users_model->count_items_where($seedling_table, $seedling_where,$seedling_select);




$start_date_where = 'task_costs.task_id = task.task_id AND task.project_id = '.$project_id;
$start_date_table = 'task,task_costs';
$start_date_select = 'min(task_costs.start_date) as number';
$min_start_date =  $this->users_model->count_items_where($start_date_table, $start_date_where,$start_date_select);

$start_date_where = 'task_costs.task_id = task.task_id AND task.project_id = '.$project_id;
$start_date_table = 'task,task_costs';
$start_date_select = 'max(task_costs.end_date) as number';
$max_end_date =  $this->users_model->count_items_where($start_date_table, $start_date_where,$start_date_select);


$used_where = 'task_casual.payment_status = 1 AND task_costs.task_id = task.task_id AND task_casual.task_cost_id = task_costs.task_cost_id AND task.project_id = '.$project_id;
$used_table = 'task,task_costs,task_casual';
$used_select = 'SUM(amount) AS number';
$used_money  =  $this->users_model->count_items_where($used_table, $used_where,$used_select);


$used_where = 'task_casual.payment_status = 1 AND task_costs.task_id = task.task_id AND task_casual.task_cost_id = task_costs.task_cost_id AND task.project_id = '.$project_id;
$used_table = 'task,task_costs,task_casual';
$min_date_select = 'min(task_casual.date_saved) as number';
$actual_start_date =  $this->users_model->count_items_where($used_table, $used_where,$min_date_select);

$account_balance = $total_budgeted_mount - $used_money;


if($account_balance > 0)
{
	$account_balance = 'Project is within budget balance '.$account_balance;
}
else
{
	$account_balance = 'Project has exceeded budget balance '.$account_balance;
}


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | <?php echo $project_title?> REPORT</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_top_border{border-top: #888888 medium solid; margin-top:1px;}
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			body
			{
				font-size: 10px;
			}
			p
			{
				line-height:6px;
			}
		</style>
    </head>
    <body >
        <div class="row">
	        <div  class="col-xs-10  col-xs-offset-1">
		    	<div class="receipt_bottom_border">
		        	<div class="row">
			        	<div class="col-xs-12">
			            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
			            </div>
			        </div>
			    	<div class="row">
			        	<div class="col-md-12 center-align">
			            	<strong>
			                	<?php echo $contacts['company_name'];?><br/>
			                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
			                    E-mail: <?php echo $contacts['email'];?>.
			                   
			                </strong>
			            </div>
			        </div>
		        </div>

		        <div class="row receipt_bottom_border">
		        	<div class="col-md-12 center-align">
		            	<strong>
		            		<h5><?php echo strtoupper($project_title);?> <br> SUMMARY REPORT</h5>
		            	</strong>
		            </div>
		        </div>
		    	
		        <div class="row receipt_bottom_border" style="margin-bottom: 10px;margin-top: 5px; font-size: 10px;">
		        	<div class="col-xs-12 ">
		                <table class="table table-bordered table-striped table-condensed">
							<tbody>
								<tr>
									<td class="col-md-8">Date/Year :</td> <td class="center-align"><?php echo date('Y-m-d')?></td>
								</tr>
								
								<tr>
									<td class="col-md-8">No of Tasks:</td> <td class="center-align"><?php echo $total_task?></td>
								</tr>
								<tr>
									<td class="col-md-8">No of Casuals:</td> 
									<td>
										  <table class="table table-no-border table-condensed">
											<tbody>
												<tr>
													<td class="col-md-4">Male :</td> <td class="center-align"><?php echo $total_communities_member_male?></td>
												</tr>
												<tr>
													<td class="col-md-4">Female :</td> <td class="center-align"><?php echo $total_communities_member_female?></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td class="col-md-8">Total Budgeted Amount:</td> <td class="center-align"><?php echo $total_budgeted_mount;?></td>
								</tr>
								<tr>
									<td class="col-md-8">Total Budgeted Time:</td> 
									<td>
										  <table class="table table-no-border table-condensed">
											<tbody>
												<tr>
													<td class="col-md-4">Start Date :</td> <td class="center-align"><?php echo $min_start_date?></td>
												</tr>
												<tr>
													<td class="col-md-4">End Date :</td> <td class="center-align"><?php echo $max_end_date?></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td class="col-md-8">Actual Amount Used:</td> <td class="center-align"><?php echo $used_money;?></td>
								</tr>
								<tr>
									<td class="col-md-8">Actual Project Start Date:</td> <td class="center-align"><?php echo $actual_start_date;?></td>
								</tr>
								<tr>
									<td class="col-md-8">Amount Balance:</td> <td class="center-align"><?php echo $account_balance;?></td>
								</tr>
							</tbody>
		                </table>
		                 	

		               
		            </div>
		        </div>
		       <div class="row" >
		       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
		       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
		                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
		            </div>
		       </div>
		       
		</div>
		</div>
        
    </body>
    
</html>

