<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Project Title</th>
						<th>Project Financier</th>
						<th>Grant County</th>
						<th>Created</th>
						<th>Status</th>
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$project_id = $row->project_id;
				$project_number = $row->project_number;
				$project_title = $row->project_title;
				$project_grant_value = $row->project_grant_value;
				$project_location = $row->project_location;
				$project_status = $row->project_status;
				$project_donor = $row->project_donor;
				$county_name = $row->county_name;
			
				$project_instructions = $row->project_instructions;
				
				$created_by = $row->created_by;
				$created = $row->created;
				$modified_by = $row->modified_by;
				$last_modified = $row->last_modified;
				$project_approval_status = $row->project_approval_status;
				$project_start_date = $row->project_start_date;
				$project_end_date = $row->project_end_date;
				
				//status
				if($project_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				//status
				
				//create deactivated status display
				if($project_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-danger" href="'.site_url().'projects/activate-project/'.$project_id.'" onclick="return confirm(\'Do you want to activate '.$project_title.'?\');" title="Activate '.$project_title.'"><i class="fa fa-thumbs-up"></i> Activate</a>';
					$button_tasks='';
					$button_edit='';

				
				}
				//create activated status display
				else if($project_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'projects/deactivate-project/'.$project_id.'" onclick="return confirm(\'Do you want to Deactivate '.$project_title.'?\');" title="Deactivate '.$project_title.'"><i class="fa fa-thumbs-down"></i> Deactivate</a>';
					$button_tasks = '<a class="btn btn-success" href="'.site_url().'projects/project-tasks/'.$project_id.'" title=" Tasks '.$project_title.'"><i class="fa fa-file"></i> Tasks</a>';
			        $button_edit = '<a class="btn btn-warning" href="'.site_url().'projects/project-edit/'.$project_id.'" title=" Tasks '.$project_title.'"><i class="fa fa-pencil"></i> Edit</a>';
			
				
				}
				
				
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$project_title.'</td>
						<td>'.$project_donor.'</td>
						<td>'.$county_name.'</td>
						<td>'.$created.'</td>
						<td>'.$status.'</td>			
						<td>'.$button.'</td>
						<td>'.$button_tasks.'</td>
						<td>'.$button_edit.'</td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no Projects";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				<div class="row" style="margin-bottom:20px;">
                                    <div class="col-lg-2 col-lg-offset-10">
                                        <a href="<?php echo site_url();?>projects/add-project" class="btn btn-sm btn-info pull-right">Add Project</a>
                                     </div>
                                   
                                </div>

				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

