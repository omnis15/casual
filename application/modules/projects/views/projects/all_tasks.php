<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Task Name</th>
						<th>Estimated Budget</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Created</th>
						<th>Status</th>
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$task_id = $row->task_id;
				$task_name = $row->task_name;
				$task_budget = $this->projects_model->get_budget($task_id);
				$start_date = $this->projects_model->get_min_start_date($task_id);
				$end_date = $this->projects_model->get_max_end_date($task_id);
				$created_by = $row->created_by;
				$created = $row->created;
				$project_status = $row->task_status;

				
				
				//status
				if($project_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				//status
				
				//create deactivated status display
				if($project_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-danger" href="'.site_url().'projects/activate-task/'.$project_id.'/'.$task_id.'" onclick="return confirm(\'Do you want to activate '.$task_name.'?\');" title="Activate '.$task_name.'"><i class="fa fa-thumbs-up"></i> Activate</a>';
					$button_tasks='';
					$button_edit='';

				
				}
				//create activated status display
				else if($project_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'projects/deactivate-task/'.$project_id.'/'.$task_id.'" onclick="return confirm(\'Do you want to Deactivate '.$task_name.'?\');" title="Deactivate '.$task_name.'"><i class="fa fa-thumbs-down"></i> Deactivate</a>';
					$button_tasks = '<a class="btn btn-warning" href="'.site_url().'projects/project-tasks-costs/'.$project_id.'/'.$task_id.'" title=" Casual '.$task_name.'"><i class="fa fa-user"></i> Casual Roles</a>';
			        $button_edit = '<a class="btn btn-warning" href="'.site_url().'projects/edit-task/'.$project_id.'/'.$task_id.'" title=" Casual '.$task_name.'"><i class="fa fa-pencil"></i> Edit</a>';
			        
				
				}
				
				
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$task_name.'</td>
						<td>'.$task_budget.'</td>
						<td>'.$start_date.'</td>
						<td>'.$end_date.'</td>
						<td>'.$created.'</td>
						<td>'.$status.'</td>			
						<td>'.$button.'</td>
						<td>'.$button_edit.'</td>
						<td>'.$button_tasks.'</td>
						
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no Tasks";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				<div class="row" style="margin-bottom:20px;">
                                    <div class="col-lg-2 col-lg-offset-10">
                                        <a href="<?php echo site_url();?>projects/project-tasks-add/<?php echo $project_id?>" class="btn btn-sm btn-info pull-right">Add Tasks</a>
                                     </div>
                                   
                                </div>

				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

