<?php
//personnel data
$task_cost_name = set_value('task_cost_name');
$task_casual_cost = set_value('task_casual_cost');
$operation_id = set_value('operation_id');
$start_date = set_value('start_date');
$end_date = set_value('end_date');
//$casual_number = set_value('casual_number');
$operation_number = set_value('operation_number');




?>   
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>projects" class="btn btn-info pull-right">Back to Projects</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                        $success = $this->session->userdata('success_message');
                        $error = $this->session->userdata('error_message');
                        
                        if(!empty($success))
                        {
                            echo '
                                <div class="alert alert-success">'.$success.'</div>
                            ';
                            
                            $this->session->unset_userdata('success_message');
                        }
                        
                        if(!empty($error))
                        {
                            echo '
                                <div class="alert alert-danger">'.$error.'</div>
                            ';
                            
                            $this->session->unset_userdata('error_message');
                        }
                        $validation_errors = validation_errors();
                        
                        if(!empty($validation_errors))
                        {
                            echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                        }
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
       

        
        <div class="form-group">
            <label class="col-lg-5 control-label">Role Name: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="task_cost_name" placeholder="Role Name" value="<?php echo $task_cost_name;?>">
            </div>
        </div>
       
        <div class="form-group">
             <label class="col-lg-5 control-label">Operation: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="operation_id">
                    <option value="">--Select Operation--</option>
                    <?php
                        if($Operation->num_rows() > 0)
                        {
                            $veh = $Operation->result();
                            
                            foreach($veh as $res)
                            {
                                $operation_id = $res->operation_id;
                                $operation_name = $res->operation_name;
                                
                                
                              
                                echo '<option value="'.$operation_id.'">'.$operation_name.'</option>';
                                
                            }
                        }
                    ?>
                </select>
             </div>
        </div>
         <div class="form-group">
            <label class="col-lg-5 control-label">Role Cost : </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="task_casual_cost" placeholder="Per operation" value="<?php echo $task_casual_cost;?>">
            </div>
        </div> 
        
    </div>
    
    <div class="col-md-6">
		<div class="form-group">
		            <label class="col-lg-5 control-label">Start Date : </label>
		            
		            <div class="col-lg-7">
		            	<div class="input-group">
		                    <span class="input-group-addon">
		                        <i class="fa fa-calendar"></i>
		                    </span>
		                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="start_date" placeholder="Start of Role Date" value="<?php echo $start_date;?>">
		                </div>
		            </div>
		 </div>
		 <div class="form-group">
		            <label class="col-lg-5 control-label">End Date : </label>
		            
		            <div class="col-lg-7">
		            	<div class="input-group">
		                    <span class="input-group-addon">
		                        <i class="fa fa-calendar"></i>
		                    </span>
		                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="end_date" placeholder="End Of Role Date" value="<?php echo $end_date;?>">
		                </div>
		            </div>
		 </div>
		 <div class="form-group">
            <label class="col-lg-5 control-label">Operation Number: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="operation_number" placeholder="Per operation" value="<?php echo $operation_number;?>">
            </div>
        </div>

    </div>

    

</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add Role
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>