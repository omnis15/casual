<?php

class Counties_model extends CI_Model 
{	
	/*
	*	Retrieve all customers
	*
	*/
	public function get_all_counties()
	{
		//retrieve all users
		$this->db->from('counties');
		$this->db->select('*');
		$this->db->where('county_status > 0');
		
		$query = $this->db->get('');
		
		return $query;
	}
	public function get_operation_name()
	{
		//retrieve all users
		$this->db->from('operation');
		$this->db->select('*');
		$this->db->where('operation_status > 0');
		
		$query = $this->db->get('');
		
		return $query;
	}
	public function get_operation_type()
	{
		//retrieve all users
		$this->db->from('operation_type');
		$this->db->select('*');
		$this->db->where('operation_type_status > 0');
		
		$query = $this->db->get('');
		
		return $query;
	}
	public function get_one_operation_type_name($operation_type_id)
	{
		$total_batch='0';
		$this->db->select('operation_type_name');
		$this->db->where('operation_type_status > 0 AND operation_type_id = '.$operation_type_id);
		$query = $this->db->get('operation_type');
		$total = $query->row();
		$total_batch = $total->operation_type_name;
		
		
		return $total_batch;
	}
	public function get_all_counties_table($table, $where, $per_page, $page, $order = 'county_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_all_operations_table($table, $where, $per_page, $page, $order = 'operation_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function add_county()
	{
		$data = array(
				'county_name'=>$this->input->post('county_name'),
				'county_status'=>1

				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('counties', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		
		}
	}
	public function add_operation()
	{
		$data = array(
				'operation_name'=>$this->input->post('operation_name'),
				'operation_type_id'=>$this->input->post('operation_type_id'),
				'operation_status'=>1

				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('operation', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		
		}
	}
	public function deactivate_operation($operation_id)
	{
		$data = array(
				'operation_status' => 0
			);
		$this->db->where('operation_id', $operation_id);
		
		if($this->db->update('operation', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function deactivate_county($county_id)
	{
		$data = array(
				'county_status' => 0
			);
		$this->db->where('county_id', $county_id);
		
		if($this->db->update('counties', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	
}
?>