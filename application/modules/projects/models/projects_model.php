<?php

class Projects_model extends CI_Model 
{	
	/*
	*	Retrieve all customers
	*
	*/
	public function get_all_projects($table, $where, $per_page, $page, $order = 'project_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function add_project()
	{
		$data = array(
				'project_title'=>$this->input->post('project_title'),
				'project_donor'=>$this->input->post('project_financier'),
				'project_instructions'=>$this->input->post('project_description'),
				'project_grant_county'=>$this->input->post('project_county'),
				'project_grant_value'=>$this->input->post('project_value'),
				'created_by'=>$this->session->userdata('personnel_id'),
				'project_status'=>1

				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('projects', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		
		}
	}
	public function get_all_tasks($table, $where, $per_page, $page, $order = 'task_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function add_task($project_id)
	{
		$data = array(
				'task_name'=>$this->input->post('task_name'),
				'task_description'=>$this->input->post('task_description'),
				'personnel_allocated_id'=>$this->input->post('allocated_personnel_id'),
				'created_by'=>$this->session->userdata('personnel_id'),
				'task_status'=>1,
				'project_id'=>$project_id

				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('task', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		
		}
	}
	public function get_all_task_costs($table, $where, $per_page, $page, $order = 'task_cost_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function add_task_costs($task_id)
	{
		$start_date =$this->input->post('start_date');
		$end_date =$this->input->post('end_date');

		$startDate = strtotime($start_date);
		$endDate = strtotime($end_date);

		$interval = $endDate - $startDate;
		$task_time = 1 + floor($interval / (60 * 60 * 24));



		$data = array(
				'task_cost_name'=>$this->input->post('task_cost_name'),
				'start_date'=>$this->input->post('start_date'),
				'end_date'=>$this->input->post('end_date'),

				'casual_number'=>1,
				'task_casual_cost'=>$this->input->post('task_casual_cost'),
				'operation_id'=>$this->input->post('operation_id'),
				'operation_number'=>$this->input->post('operation_number'),

				'created_by'=>$this->session->userdata('personnel_id'),
				'task_cost_status'=>1,
				'task_id'=>$task_id,
				'task_time'=>$task_time

				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('task_costs', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		
		}
	}
	public function edit_project($project_id)
	{
		$data = array(
                'project_title'=>$this->input->post('project_title'),
				'project_donor'=>$this->input->post('project_financier'),
				'project_instructions'=>$this->input->post('project_description'),
				'project_grant_county'=>$this->input->post('project_county'),
				'project_grant_value'=>$this->input->post('project_value'),
				'created_by'=>$this->session->userdata('personnel_id'),
				'project_status'=>1
				
			);
			
		$this->db->where('project_id', $project_id);
		if($this->db->update('projects', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_project($project_id)
	{
		//retrieve all users
		$this->db->from('projects');
		$this->db->select('*');
		$this->db->where('project_id = '.$project_id);
		$query = $this->db->get();
		
		return $query;
	}
	public function edit_task($task_id)
	{
		$data = array(
               'task_name'=>$this->input->post('task_name'),
				'task_description'=>$this->input->post('task_description'),
				'personnel_allocated_id'=>$this->input->post('allocated_personnel_id'),
				'created_by'=>$this->session->userdata('personnel_id'),
				'task_status'=>1,
				
			);
			
		$this->db->where('task_id', $task_id);
		if($this->db->update('task', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_task($task_id)
	{
		//retrieve all users
		$this->db->from('task');
		$this->db->select('*');
		$this->db->where('task_id = '.$task_id);
		$query = $this->db->get();
		
		return $query;
	}


	public function deactivate_project($project_id)
	{
		$data = array(
				'project_status' => 0
			);
		$this->db->where('project_id', $project_id);
		
		if($this->db->update('projects', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function activate_project($project_id)
	{
		$data = array(
				'project_status' => 1
			);
		$this->db->where('project_id', $project_id);
		
		if($this->db->update('projects', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function deactivate_task($task_id)
	{
		$data = array(
				'task_status' => 0
			);
		$this->db->where('task_id', $task_id);
		
		if($this->db->update('task', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function activate_task($task_id)
	{
		$data = array(
				'task_status' => 1
			);
		$this->db->where('task_id', $task_id);
		
		if($this->db->update('task', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function deactivate_task_cost($task_cost_id)
	{
		$data = array(
				'task_cost_status' => 0
			);
		$this->db->where('task_cost_id', $task_cost_id);
		
		if($this->db->update('task_costs', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_budget($task_id)
	{
		$product_name='0';
		$this->db->select('SUM(task_casual_cost*operation_number*task_time) as product_name');
		$this->db->where('task_cost_status = 1 AND task_id = '.$task_id);
		$query = $this->db->get('task_costs');
		$total = $query->row();
		$product_name = $total->product_name;
		return $product_name;
	}
	public function get_max_end_date($task_id)
	{
		$product_name='0';
		$this->db->select('MAX(end_date) as product_name');
		$this->db->where('task_cost_status = 1 AND task_id = '.$task_id);
		$query = $this->db->get('task_costs');
		$total = $query->row();
		$product_name = $total->product_name;
		return $product_name;
	}
	public function get_min_start_date($task_id)
	{
		$product_name='0';
		$this->db->select('MIN(start_date) as product_name');
		$this->db->where('task_cost_status = 1 AND task_id = '.$task_id);
		$query = $this->db->get('task_costs');
		$total = $query->row();
		$product_name = $total->product_name;
		return $product_name;
	}
	

























	function get_payment_methods()
	{
		$table = "payment_method";
		$where = "payment_method_id > 0";
		$items = "*";
		$order = "payment_method";
		
		$this->db->order_by($order);
		$result = $this->db->get($table);
		
		return $result;
	}
	

	public function get_customer_payments($customer_id)
	{

		$this->db->where('customer_id = '.$customer_id);
		$query = $this->db->get('jopet_payments');
		
		return $query;
	}
	public function get_customer_yard_payments($customer_id)
	{

		$this->db->where('customer_id = '.$customer_id);
		$query = $this->db->get('yard_jopet_payments');
		
		return $query;
	}
	public function all_customers()
	{
		$this->db->where('customer_status = 1');
		$query = $this->db->get('customer');
		
		return $query;
	}
	public function one_customer($customer_id)
	{
		$this->db->where('customer_status = 1 AND customer_id = '.$customer_id.'');
		$query = $this->db->get('customer');
		
		return $query;
	}
	public function get_product_name($product_id)
	{
		$product_name='0';
		$this->db->select('product_name');
		$this->db->where('product_id = 1');
		$query = $this->db->get('products');
		$total = $query->row();
		$product_name = $total->product_name;
		return $product_name;
	}
	public function all_products()
	{
		$this->db->where('product_status = 1');
		$query = $this->db->get('products');
		
		return $query;
	}
	public function all_sales($customer_id)
	{
		$this->db->where('sale_status = 1 AND customer_id = '.$customer_id.'');
		$query = $this->db->get('sales');
		
		return $query;
	}
	public function all_sales_yard($customer_id)
	{
		$this->db->where('sale_status = 1 AND customer_id = '.$customer_id.'');
		$query = $this->db->get('yard_sales');
		
		return $query;
	}
	public function get_total_batch()
	{
		$total_sales='0';
		$this->db->select('SUM(sales.sale_quantity) AS total_sales');
		$this->db->where('sales.sale_status = 1 AND sales.product_id = 1');
		$query2 = $this->db->get('sales');
		$total2 = $query2->row();
		$total_sales = $total2->total_sales;
		
		return $total_sales;
	}
	
	/*
	*	Retrieve all ride types
	*
	*/
	public function get_ride_types()
	{
		$this->db->order_by('ride_type_name');
		$query = $this->db->get('ride_type');
		
		return $query;
	}
	
	/*
	*	Retrieve all customers
	*	@param string $table
	* 	@param string $where
	*
	*/
	


	
	public function get_all_customer_contacts($table, $where, $per_page, $page, $order = 'customer_contacts_first_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	function create_receipt_number()
	{
		//select product code
		$preffix = "JOP-RN_";
		$this->db->from('jopet_payments');
		$this->db->where("reciept_number LIKE '".$preffix."%' AND status = 1");
		$this->db->select('MAX(reciept_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	function create_receipt_number_yard()
	{
		//select product code
		$preffix = "JOP-Y-RN_";
		$this->db->from('yard_jopet_payments');
		$this->db->where("reciept_number LIKE '".$preffix."%' AND status = 1");
		$this->db->select('MAX(reciept_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	function create_cat_number()
	{
		//select product code
		$preffix = "JOP-CAT_";
		$this->db->from('cash_at_hand');
		$this->db->where("reciept_number LIKE '".$preffix."%' AND cat_status = 1");
		$this->db->select('MAX(reciept_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	
	

	/*
	*	Add a new customer
	*	@param string $image_name
	*
	*/
	public function receipt_payment($customer_id)
	{
		$amount = $this->input->post('amount_paid');
		$paid_by = $this->input->post('paid_by');
		$payment_method=$this->input->post('payment_method');
		$type_of_account=$this->input->post('type_of_account');
		
		
		
		if($payment_method == 1)
		{
			$transaction_code = $this->input->post('bank_name');
		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else
		{
			$transaction_code = '';
		}
		$receipt_number = $this->accounting_model->create_receipt_number();

		$data = array(
			'payment_method_id'=>$payment_method,
			'amount'=>$amount,
			'transaction_code'=>$transaction_code,
			'reciept_number'=>$receipt_number,
			'paid_by'=>$this->input->post('paid_by'),
			'status'=>1,
			'customer_id'=>$customer_id
			
		);
		if ($this->db->insert('jopet_payments',$data)){


				
				return TRUE;
			}
			else{
				return FALSE;
			}
	}
	public function receipt_payment_yard($customer_id)
	{
		$amount = $this->input->post('amount_paid');
		$paid_by = $this->input->post('paid_by');
		$payment_method=$this->input->post('payment_method');
		$type_of_account=$this->input->post('type_of_account');
		
		
		
		if($payment_method == 1)
		{
			$transaction_code = $this->input->post('bank_name');
		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else
		{
			$transaction_code = '';
		}
		$receipt_number = $this->accounting_model->create_receipt_number_yard();

		$data = array(
			'payment_method_id'=>$payment_method,
			'amount'=>$amount,
			'transaction_code'=>$transaction_code,
			'reciept_number'=>$receipt_number,
			'paid_by'=>$this->input->post('paid_by'),
			'status'=>1,
			'customer_id'=>$customer_id
			
		);
		if ($this->db->insert('yard_jopet_payments',$data)){


				
				return TRUE;
			}
			else{
				return FALSE;
			}
	}
	public function add_company_contact($company_id)
	{
		$data = array(
				'customer_contacts_first_name'=>$this->input->post('customer_contacts_first_name'),
				'customer_contacts_sur_name'=>$this->input->post('customer_contacts_sur_name'),
				'customer_contacts_phone'=>$this->input->post('customer_contacts_phone'),
				'customer_contacts_email'=>$this->input->post('customer_contacts_email'),
				'customer_id'=>$company_id
				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function add_sale($customer_id)
	{


		$total_batch='0';
		$this->db->select('SUM(batches.batch_quantity) AS total_batch');
		$this->db->where('batches.batch_status = 1 AND batches.product_id = 1');
		$query = $this->db->get('batches');
		$total = $query->row();
		$total_batch = $total->total_batch;


		$total_sales='0';
		$this->db->select('SUM(sales.sale_quantity) AS total_sales');
		$this->db->where('sales.sale_status = 1 AND sales.product_id = 1');
		$query2 = $this->db->get('sales');
		$total2 = $query2->row();
		$total_sales = $total2->total_sales;



    
		$data = array(
				
				'sale_price'=>$this->input->post('sale_price'),
				'sale_quantity'=>$this->input->post('sale_quantity'),
				'sale_description'=>$this->input->post('sale_description'),
				'product_id'=>$this->input->post('product_id'),
				'customer_id'=>$customer_id,
				'sale_status'=>1
				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);

		$product_id = $this->input->post('product_id');
		$sale_quantity = $this->input->post('sale_quantity');

		$total_batch='0';
		$this->db->select('SUM(batches.batch_quantity) AS total_batch');
		$this->db->where('batches.batch_status = 1 AND batches.product_id = '.$product_id.'');
		$query = $this->db->get('batches');
		$total = $query->row();
		$total_batch = $total->total_batch;


		$total_sales='0';
		$this->db->select('SUM(sales.sale_quantity) AS total_sales');
		$this->db->where('sales.sale_status = 1 AND sales.product_id = '.$product_id.'');
		$query2 = $this->db->get('sales');
		$total2 = $query2->row();
		$total_sales = $total2->total_sales;

		$total_account = $total_batch-$total_sales-$sale_quantity;
		if($total_account>=0){
			
			if($this->db->insert('sales', $data))
			{
				$result["status"] = True;
				$result["message"] = "Sales Records Saved";
				
			}
			else{
				$result["status"] = FALSE;
				$result["message"] = "Sales Records Could not be saved";
				
			}
		}else{
			$result["status"] = FALSE;
			$result["message"] = "Sales Records Could not be saved, Because no Batch";
		}
		return $result;
	}
	
	
	/*
	*	Add a new add_sale
	*/
	public function add_customer($is_company)
	{
		if($is_company == 0){
			$one = '0';
		}else{
			$one = '1';
		}
		$data = array(

				'customer_surname'=>$this->input->post('customer_surname'),
				'customer_first_name'=>$this->input->post('customer_first_name'),
				'customer_phone'=>$this->input->post('customer_phone'),
				'customer_email'=>$this->input->post('customer_email'),
				'customer_post_code'=>$this->input->post('customer_post_code'),
				'customer_address'=>$this->input->post('customer_address'),
				'customer_created'=>date('Y-m-d H:i:s'),
				'company_status'=>$one


				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	
	/*
	*	Update an existing customer
	*	@param string $image_name
	*	@param int $customer_id
	*
	*/
	public function update_customer($customer_id)
	{
		$data = array(

				'customer_surname'=>$this->input->post('customer_surname'),
				'customer_first_name'=>$this->input->post('customer_first_name'),
				'customer_phone'=>$this->input->post('customer_phone'),
				'customer_email'=>$this->input->post('customer_email'),
				'customer_post_code'=>$this->input->post('customer_post_code'),
				'customer_address'=>$this->input->post('customer_address'),
				
			);
			
		$this->db->where('customer_id', $customer_id);
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_customer($customer_id)
	{
		//retrieve all users
		$this->db->from('customer');
		$this->db->select('*');
		$this->db->where('customer_id = '.$customer_id);
		$query = $this->db->get();
		
		return $query;
	}
	





/*
	*	Update an existing customer
	*	@param string $image_name
	*	@param int $customer_id
	*
	*/
	public function update_company_contact($customer_contact_id) 
	{

		$data = array(
			
				'customer_contacts_first_name'=>$this->input->post('customer_contacts_first_name'),
				'customer_contacts_sur_name'=>$this->input->post('customer_contacts_sur_name'),
				'customer_contacts_phone'=>$this->input->post('customer_contacts_phone'),
				'customer_contacts_email'=>$this->input->post('customer_contacts_email'),
				
				
			);
			
		$this->db->where('customer_contacts_id', $customer_contact_id);
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_customer_contact($customer_contact_id)
	{
		//retrieve all users
		$this->db->from('customer_contacts');
		$this->db->select('*');
		$this->db->where('customer_contacts_id = '.$customer_contact_id);
		$query = $this->db->get();
		
		return $query;
	}
	/*
	*	Activate a deactivated customer
	*	@param int $customer_id
	*
	*/
	public function activate_customer_contact($customer_contact_id)
	{
		$data = array(
				'customer_contacts_status' => 1
			);
		$this->db->where('customer_contacts_id', $customer_contact_id);
		
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated customer
	*	@param int $customer_id
	*
	*/
	public function deactivate_customer_contact($customer_contact_id)
	{
		$data = array(
				'customer_contacts_status' => 0
			);
		$this->db->where('customer_contacts_id', $customer_contact_id);
		
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Delete an existing customer
	*	@param int $customer_id
	*
	*/
	public function delete_customer($customer_id)
	{
		if($this->db->delete('customer', array('customer_id' => $customer_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated customer
	*	@param int $customer_id
	*
	*/
	public function activate_customer($customer_id)
	{
		$data = array(
				'customer_status' => 1
			);
		$this->db->where('customer_id', $customer_id);
		
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated customer
	*	@param int $customer_id
	*
	*/
	public function deactivate_customer($customer_id)
	{
		$data = array(
				'customer_status' => 0
			);
		$this->db->where('customer_id', $customer_id);
		
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	/*
	*	Retrieve all project items of an project
	*
	*/
	public function get_project_detail($project_id)
	{
		$this->db->select('*');
		$this->db->where('projects.project_id = '.$project_id);
		$query = $this->db->get('projects');
		
		return $query;
	}
	
	/*
	*	Retrieve personnel
	*
	*/
	public function get_personnel_type($personnel_type_id)
	{
		$this->db->where(array('personnel_status' => 1, 'personnel_type_id' => $personnel_type_id));
		$query = $this->db->get('personnel');
		
		return $query;
	}

	public function get_project_title($project_id)
	{
		$this->db->where('project_id = '.$project_id.'');
		$query = $this->db->get('projects');

		$result = $query->result();

		$project_title = $result[0]->project_title;
		return $project_title;
	}
	public function get_project_content($table, $where,$select,$group_by=NULL)
	{
		$this->db->from($table);
		$this->db->select($select);
		$this->db->where($where);
		if($group_by != NULL)
		{
			$this->db->group_by($group_by);
		}
		$query = $this->db->get('');
		
		return $query;
	}
}
?>