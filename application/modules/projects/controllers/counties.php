<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Counties extends MX_Controller 
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
	
		$this->load->model('projects_model');
		$this->load->model('counties_model');
		$this->load->model('accounts/accounts_model');
		
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
    
	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function index($order = 'county_name', $order_method = 'ASC') 
	{

		$where = 'counties.county_status > 0 ';
		$table = ' counties';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->counties_model->get_all_counties_table($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Counties';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('projects/projects/all_counties', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	/*
	*
	*	Add a new project
	*
	*/
	public function add_county(){
		
		//form validation rules
		$this->form_validation->set_rules('county_name', 'County Name', 'required|xss_clean');
		

		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->counties_model->add_county()){
				$this->session->set_userdata('success_message', 'County added successfully');
				redirect('project-administration/counties');
			}
			
			else{
				$this->session->set_userdata('error_message', 'Could not add County. Please try again');
			}
		}
		
	
		
		$data['title'] = 'Add County';
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('projects/projects/add_county', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function delete_county($county_id)
	{
		if($this->counties_model->deactivate_county($county_id))
		{
			$this->session->set_userdata('success_message', 'County Deleted successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'County could not be Deleted. Please try again');
		}
		redirect('project-administration/counties');
	}
	public function operation_index($order = 'operation_name', $order_method = 'ASC') 
	{

		$where = 'operation.operation_status > 0 ';
		$table = ' operation';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->counties_model->get_all_operations_table($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Operations';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('projects/projects/all_operations', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	/*
	*
	*	Add a new project
	*
	*/
	public function add_operation(){
		
		//form validation rules
		$this->form_validation->set_rules('operation_name', 'Operation Name', 'required|xss_clean');
		$this->form_validation->set_rules('operation_type_id', 'Operation Type', 'required|xss_clean');
		

		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->counties_model->add_operation()){
				$this->session->set_userdata('success_message', 'Operation added successfully');
				redirect('project-administration/operations');
			}
			
			else{
				$this->session->set_userdata('error_message', 'Could not add Operation. Please try again');
			}
		}
		
	
		
		$data['title'] = 'Add Operation';
		$v_data['title'] = $data['title'];
		$v_data['products'] = $this->counties_model->get_operation_type();
		
		$data['content'] = $this->load->view('projects/projects/add_operation', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function delete_operation($operation_id)
	{
		if($this->counties_model->deactivate_operation($operation_id))
		{
			$this->session->set_userdata('success_message', 'Operation Deleted successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Operation could not be Deleted. Please try again');
		}
		redirect('project-administration/operations');
	}


	
}
?>