<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Casuals extends MX_Controller 
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
	
		$this->load->model('projects/projects_model');
		$this->load->model('projects/counties_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('sms/sms_model');
		$this->load->model('financials/financials_model');
		$this->load->model('casuals_model');
		

		
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
    
	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function index($order = 'project_title', $order_method = 'ASC') 
	{

		$where = 'projects.project_id > 0 AND counties.county_id = projects.project_grant_county';
		$table = 'projects, counties';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->projects_model->get_all_projects($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Projects';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('casuals/casuals/all_projects', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	/*
	*
	*	Add a new project
	*
	*/
	public function add_project(){
		
		//form validation rules
		$this->form_validation->set_rules('project_title', 'Project Title', 'required|xss_clean');
		$this->form_validation->set_rules('project_financier', 'Project Financier', 'required|xss_clean');
		$this->form_validation->set_rules('project_description', 'Project Description', 'required|xss_clean');
		$this->form_validation->set_rules('project_county', 'Project County', 'required|xss_clean');
		$this->form_validation->set_rules('project_value', 'Project Value', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->projects_model->add_project()){
				$this->session->set_userdata('success_message', 'Project added successfully');
				redirect('projects');
			}
			
			else{
				$this->session->set_userdata('error_message', 'Could not add Project. Please try again');
			}
		}
		
	
		
		$data['title'] = 'Add Project';
		$v_data['title'] = $data['title'];
		$v_data['counties'] = $this->counties_model->get_all_counties();
	
		$data['content'] = $this->load->view('projects/projects/add_project', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function edit_project($project_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('project_title', 'Project Title', 'required|xss_clean');
		$this->form_validation->set_rules('project_financier', 'Project Financier', 'required|xss_clean');
		$this->form_validation->set_rules('project_description', 'Project Description', 'required|xss_clean');
		$this->form_validation->set_rules('project_county', 'Project County', 'required|xss_clean');
		$this->form_validation->set_rules('project_value', 'Project Value', 'required|xss_clean');
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update vehicle
			if($this->projects_model->edit_project($project_id))
			{
				$this->session->set_userdata('success_message', 'Project updated successfully');
				redirect('projects');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update Project. Please try again');
			}
		}
		
		//open the add new vehicle
		$data['title'] = 'Edit Project';
		$v_data['title'] = $data['title'];
		$v_data['counties'] = $this->counties_model->get_all_counties();
		$v_data['project_id'] = $project_id;
		$customer = $this->projects_model->get_project($project_id);
		$v_data['customer'] = $customer->result();
		
	
		$data['content'] = $this->load->view('projects/projects/edit_projects', $v_data, true);
		
		
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Deactivate an existing Customer
	*	@param int $vehicle_id
	*
	*/
	public function deactivate_project($project_id)
	{
		if($this->projects_model->deactivate_project($project_id))
		{
			$this->session->set_userdata('success_message', 'Project disabled successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Project could not be disabled. Please try again');
		}
		redirect('projects');
	}
	/*
	*
	*	Activate an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function activate_project($project_id)
	{
		if($this->projects_model->activate_project($project_id))
		{
			$this->session->set_userdata('success_message', 'Customer activated successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Customer could not be activated. Please try again');
		}
		redirect('projects');
	}
	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function all_tasks($project_id) 
	{
		$order = 'task_name';
		$order_method = 'ASC';

		$where = 'task.task_id > 0 AND task.project_id = '.$project_id;
		$table = 'task';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->projects_model->get_all_tasks($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Tasks';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['project_id'] = $project_id;
		

		$data['content'] = $this->load->view('financials/financials/all_tasks', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Add a new project
	*
	*/
	public function add_task($project_id){
		
		//form validation rules
		$this->form_validation->set_rules('task_name', 'Task Name', 'required|xss_clean');
		$this->form_validation->set_rules('task_description', 'Task Description', 'required|xss_clean');
		$this->form_validation->set_rules('allocated_personnel_id', 'Allocated Personnel', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->projects_model->add_task($project_id)){
				$this->session->set_userdata('success_message', 'Task added successfully');
				redirect('projects/project-tasks/'.$project_id);
			}
			
			else{
				$this->session->set_userdata('error_message', 'Could not add Task. Please try again');
			}
		}
		
	
		
		$data['title'] = 'Add Task';
		$v_data['title'] = $data['title'];
		$v_data['Supervisor'] = $this->personnel_model->get_ind_person_job();
		$v_data['project_id'] = $project_id;
	
		$data['content'] = $this->load->view('projects/projects/add_task', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function edit_task($project_id, $task_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('task_name', 'Task Name', 'required|xss_clean');
		$this->form_validation->set_rules('task_description', 'Task Description', 'required|xss_clean');
		$this->form_validation->set_rules('allocated_personnel_id', 'Allocated Personnel', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update vehicle
			if($this->projects_model->edit_task($task_id))
			{
				$this->session->set_userdata('success_message', 'Task updated successfully');
				redirect('projects/project-tasks/'.$project_id);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update Task. Please try again');
			}
		}
		
		//open the add new vehicle
		$data['title'] = 'Edit Task';
		$v_data['title'] = $data['title'];
		$v_data['Supervisor'] = $this->personnel_model->get_ind_person_job();
		$v_data['project_id'] = $project_id;
		$customer = $this->projects_model->get_task($task_id);
		$v_data['customer'] = $customer->result();
		
	
		$data['content'] = $this->load->view('projects/projects/edit_task', $v_data, true);
		
		
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Deactivate an existing Customer
	*	@param int $vehicle_id
	*
	*/
	public function deactivate_task($project_id,$task_id)
	{
		if($this->projects_model->deactivate_task($task_id))
		{
			$this->session->set_userdata('success_message', 'Task disabled successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Task could not be disabled. Please try again');
		}
		redirect('projects/project-tasks/'.$project_id);
	}
	/*
	*
	*	Activate an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function activate_task($project_id, $task_id)
	{
		if($this->projects_model->activate_task($task_id))
		{
			$this->session->set_userdata('success_message', 'Task activated successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Task could not be activated. Please try again');
		}
		redirect('projects/project-tasks/'.$project_id);
	}


	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function all_task_costs($project_id,$task_id) 
	{
		$order = 'task_cost_id';
		$order_method = 'ASC';

		$where = 'operation.operation_id = task_costs.operation_id AND operation.operation_type_id = operation_type.operation_type_id AND task_costs.task_cost_status > 0 AND task_costs.task_id = '.$task_id;
		$table = 'task_costs, operation, operation_type';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->projects_model->get_all_task_costs($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Task Roles';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['task_id'] = $task_id;
		$v_data['project_id'] = $project_id;
		

		$data['content'] = $this->load->view('financials/financials/all_task_costs', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Add a new project
	*
	*/
	public function add_task_costs($project_id, $task_id){
		
		//form validation rules
		$this->form_validation->set_rules('task_cost_name', 'Role Name', 'required|xss_clean');
		$this->form_validation->set_rules('task_casual_cost', 'Role Cost', 'required|xss_clean');
		$this->form_validation->set_rules('operation_id', 'Operation Name', 'required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Role Name', 'required|xss_clean');
		$this->form_validation->set_rules('end_date', 'Role Cost', 'required|xss_clean');
		$this->form_validation->set_rules('casual_number', 'Operation Name', 'required|xss_clean');
		$this->form_validation->set_rules('operation_number', 'Role Name', 'xss_clean');
		


		//if form has been submitted
		if ($this->form_validation->run())
		{
			$start_date = $this->input->post('start_date');
			$end_date =	$this->input->post('end_date');

			$startDate = strtotime($start_date);
			$endDate = strtotime($end_date);

			$interval = $endDate - $startDate;
			$task_time = 1 + floor($interval / (60 * 60 * 24));
			if($task_time == 0){
				$this->session->set_userdata('error_message', 'Could not add Task. Wrong Dates');

			}else{

				if($this->projects_model->add_task_costs($task_id)){
				$this->session->set_userdata('success_message', 'Task Role Added Successfully');
				redirect('projects/project-tasks-costs/'.$project_id.'/'.$task_id);
				}
				
				else{
					$this->session->set_userdata('error_message', 'Could not add Task. Please try again');
				}

			}



			
		}
		
	
		
		$data['title'] = 'Add Task Roles';
		$v_data['title'] = $data['title'];
		$v_data['project_id'] = $project_id;
		$v_data['task_id'] = $task_id;
		$v_data['Operation'] = $this->counties_model->get_operation_name();
	
		$data['content'] = $this->load->view('projects/projects/add_task_costs', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function deactivate_task_cost($task_cost_id)
	{
		if($this->projects_model->deactivate_task_cost($task_cost_id))
		{
			$this->session->set_userdata('success_message', 'Role Delete successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Role could not be deleted. Please try again');
		}
		redirect('projects');
		
	}

	public function all_payment_casuals($project_id) 
	{
		$order = 'casual_name';
		$order_method = 'ASC';

		$where = 'casual.casual_status = 1 AND casual.project_id = '.$project_id;
		$table = 'casual';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->financials_model->get_all_task_casuals($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Casual Data Based On Projects';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['project_id'] = $project_id;
		

		$data['content'] = $this->load->view('casuals/casuals/all_casuals', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function all_payment_list($task_cost_id) 
	{
		$order = 'task_cost_id';
		$order_method = 'ASC';

		$where = 'casual.casual_id = task_casual.casual_id AND task_casual.task_cost_id = '.$task_cost_id;
		$table = 'task_casual, casual';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->financials_model->get_all_task_costs($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Duty Rota Dates';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;

	

		$data['content'] = $this->load->view('financials/financials/all_dates', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}


	public function submit_casual_message($casual_id,$project_id)
	{

		$message = $this->input->post('message'.$casual_id);
		$phone = $this->input->post('phone'.$casual_id);

		if(!empty($message) || !empty($phone))
		{
			// send message
			if($this->sms_model->sms($message,0,$phone,1))
			{
				$this->session->set_userdata('success_message','Message successfully sent');
			}
			else
			{
				$this->session->set_userdata('error_message','Could not sent message');
			}

		}
		else
		{
			$this->session->set_userdata('error_message','Ensure you have entered a message and phone');
		}

		redirect('casuals/casuals-list/'.$project_id);
	}

	public function submit_bulk_messages($project_id)
	{
		$redirect_url = $this->input->post('redirect_url');
		$message = $this->input->post('message');

		if(!empty($message))
		{

			$order = 'project_id';
			$order_method = 'ASC';

			$where = 'casual.casual_status = 1 AND casual.project_id = '.$project_id;
			$table = 'casual';

			$this->db->where($where);
			$this->db->from($table);
			$query = $this->db->get('');

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key) {
					# code...
					$casual_id = $key->casual_id;
					$casual_name = $key->casual_name;
					$phone = $key->casual_phone;
					// $phone = 254720465220;
					$message = 'Hello '.$casual_name.' '.$message;
					$this->sms_model->sms($message,0,$phone,1);

				}
				$this->session->set_userdata('success_message','Message successfully sent');
			}
		}

		redirect('casuals/casuals-list/'.$project_id);
	}

	public function sms_auth($phone, $amount,$task_cost_id,$casual_id)
	{
		//var_dump($phone.' '.$amount.' '.$task_cost_id.' '.$casual_id);die();
		$phone_sent = $this->query_model->clean_phone_number($phone);
		$amount = 10;
		//$phone_sent = 720465220;
		if($amount > 0)
		{
           

			$fields_ser = array
						(
							'api_key' => urlencode("1000"),
							'phone_number' => urlencode($phone_sent),
							'transaction_id' => urlencode($task_cost_id),
							'amount' => urlencode($amount)
						);

			$base_url = 'https://www.omnis.co.ke/omnis_gateway/';
			$service_url = $base_url.'disburse-payment';
			$response2 = $this->query_model->rest_service($service_url, $fields_ser);
			$message = json_decode($response2);
			$message = json_decode($message);
		    if($message->result == 0)
					{
						
						$data_insert2 = array(
								'payment_status' => 1,
								'payment_date'=>date('Y-m-d H:i:s'),
							);
						$this->db->where('task_casual_id', $casual_id);
						$this->db->update('task_casual', $data_insert2);
						$this->session->set_userdata('success_message','Money successfully sent to '.$phone_sent);
						
					
						
					}
					else
					{
					
				 	    $this->session->set_userdata('error_message','Money could not be sent to '.$phone_sent);

					}

			
						
		
		}
		else
		{
			$this->session->set_userdata('error_message','Money not sent');
		}
		redirect('financials/payment-casual/'.$task_cost_id);
		
	}

	public function send_bulk($task_cost_id)
	{
		$phone_sent = $this->query_model->clean_phone_number($phone);
		$order = 'task_cost_id';
		$order_method = 'ASC';

		$where = 'casual.casual_id = task_casual.casual_id AND task_casual.task_cost_id = '.$task_cost_id;
		$table = 'task_casual, casual';

		$this->db->where($where);
		$this->db->from($table);
		$query = $this->db->get('');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
					# code...
					$casual_id = $key->casual_id;
					$casual_name = $key->casual_name;
					$phone_sent = $key->casual_phone;
					$amount = $key->amount;
					
					$amount = 10;
					$phone_sent = 720465220;
					if($amount > 0)
					{
			          
						$fields_ser = array
									(
										'api_key' => urlencode("1000"),
										'phone_number' => urlencode($phone_sent),
										'transaction_id' => urlencode($task_cost_id),
										'amount' => urlencode($amount)
									);

						$base_url = 'https://www.omnis.co.ke/omnis_gateway/';
						$service_url = $base_url.'disburse-payment';
						$response2 = $this->query_model->rest_service($service_url, $fields_ser);
						$message = json_decode($response2);
						$message = json_decode($message);
					
					}

				}

			
		}
		else
		{

			$this->session->set_userdata('success_message','Message successfully sent');
		}
		
		redirect('financials/payment-casual/'.$task_cost_id);
		
	}
	
	
}
?>