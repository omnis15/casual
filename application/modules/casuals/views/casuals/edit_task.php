
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>projects/project-tasks/<?php echo $project_id?>" class="btn btn-info pull-right">Back to Projects</a>
                        </div>
                    </div>
                    <!-- Adding Errors -->
            <?php
            if(isset($error)){
                echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
            }

            
			//the vehicle details
			$task_name = $customer[0]->task_name;
			$task_description = $customer[0]->task_description;
			$allocated_personnel_id = $customer[0]->personnel_allocated_id;
			
			
            $validation_errors = validation_errors();
            
            if(!empty($validation_errors))
            {
				$task_name = set_value('task_name');
                $task_description = set_value('task_description');
                $allocated_personnel_id = set_value('allocated_personnel_id');

				
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
			
            ?>
            
          <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
       
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Task Name: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="task_name" placeholder="Task Name" value="<?php echo $task_name;?>">
            </div>
        </div>
        <div class="form-group">
             <label class="col-lg-5 control-label">Allocated Personnel: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="allocated_personnel_id">
                    <option value="">--Select Supervisor--</option>
                    <?php
                        if($Supervisor->num_rows() > 0)
                        {
                            $veh = $Supervisor->result();
                            
                            foreach($veh as $res)
                            {
                                $allocated_personnel_id = $res->personnel_id;
                                $personnel_onames = $res->personnel_onames;
                                $personnel_fname = $res->personnel_fname;
                                $personnel_name = $personnel_fname.' '.$personnel_onames;
                                
                              
                                echo '<option value="'.$allocated_personnel_id.'">'.$personnel_name.'</option>';
                                
                            }
                        }
                    ?>
                </select>
             </div>
        </div>
        
        
    </div>
    
    <div class="col-md-6">
        
       
        
       <div class="form-group">
            <label class="col-lg-5 control-label">Task Description: </label>
            
            <div class="col-lg-7">
                 <textarea rows="4" cols="35" name="task_description" value"<?php echo $task_description?>">
                 </textarea> 
            </div>
        </div> 
    

    </div>
</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Edit Project
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>