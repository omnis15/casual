<?php
//personnel data
$customer_contacts_first_name = set_value('customer_contacts_first_name');
$customer_contacts_sur_name = set_value('customer_contacts_sur_name');
$customer_contacts_phone = set_value('customer_contacts_phone');
$customer_contacts_email = set_value('customer_contacts_email');

?>   
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>customers" class="btn btn-info pull-right">Back to Customers</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                        $success = $this->session->userdata('success_message');
                        $error = $this->session->userdata('error_message');
                        
                        if(!empty($success))
                        {
                            echo '
                                <div class="alert alert-success">'.$success.'</div>
                            ';
                            
                            $this->session->unset_userdata('success_message');
                        }
                        
                        if(!empty($error))
                        {
                            echo '
                                <div class="alert alert-danger">'.$error.'</div>
                            ';
                            
                            $this->session->unset_userdata('error_message');
                        }
                        $validation_errors = validation_errors();
                        
                        if(!empty($validation_errors))
                        {
                            echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                        }
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
       
        
        <div class="form-group">

            <label class="col-lg-5 control-label">First Name: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="customer_contacts_first_name" placeholder="First Name" value="<?php echo $customer_contacts_first_name;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">SurName: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="customer_contacts_sur_name" placeholder="Surname" value="<?php echo $customer_contacts_sur_name;?>">
            </div>
        </div>
         
        
        
    </div>
    
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-lg-5 control-label">Phone Number: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="customer_contacts_phone" placeholder="Phone Number" value="<?php echo $customer_contacts_phone;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Email Address: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="customer_contacts_email" placeholder="Email Address" value="<?php echo $customer_contacts_email;?>">
            </div>
        </div>
        
      
    

    </div>
</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add Company Contact
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>



            </section>
            <?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Phone Number</th>
						<th>Email</th>
						<th>Status</th>
						<th colspan="2">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{

				$customer_contacts_id = $row->customer_contacts_id;
				$customer_contacts_first_name = $row->customer_contacts_first_name;
				$customer_contacts_sur_name = $row->customer_contacts_sur_name;
				$customer_contacts_email = $row->customer_contacts_email;
				$customer_contacts_phone = $row->customer_contacts_phone;
				$customer_contacts_status = $row->customer_contacts_status;
				
				$customer_name = $customer_contacts_first_name.' '.$customer_contacts_sur_name;
				
				//status
				if($customer_contacts_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				
				//create deactivated status display
				if($customer_contacts_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'customers/activate_company_contact/'.$customer_contacts_id.'" onclick="return confirm(\'Do you want to activate '.$customer_name.'?\');" title="Activate '.$customer_name.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
				}
				//create activated status display
				else if($customer_contacts_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'customers/deactivate_company_contact/'.$customer_contacts_id.'" onclick="return confirm(\'Do you want to deactivate '.$customer_name.'?\');" title="Deactivate '.$customer_name.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
				}
				
				
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$customer_name.'</td>
						<td>'.$customer_contacts_phone.'</td>
						<td>'.$customer_contacts_email.'</td>
						<td>'.$status.'</td>
						<td><a href="'.site_url().'customers/edit_company_contact/'.$customer_contacts_id.'" class="btn btn-sm btn-success" title="Profile '.$customer_name.'"><i class="fa fa-pencil"></i> Edit</a></td>
						<td>'.$button.'</td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no customer";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				

				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>


  