<?php

class Loans_model extends CI_Model 
{
	/*
	*	Count all items from a table
	*	@param string $individual_id
	* 	@param string $amount
	*
	*/
	public function validate_loan($individual_id, $amount)
	{
		$maximum_amount = 10000;
		if($amount > $maximum_amount)
		{
			$return['result'] = FALSE;
			$return['message'] = 'You have exceeded the maximum loan amount of Kes '.number_format($maximum_amount);
		}
		
		else
		{
			$return['result'] = TRUE;
		}
		
		return $return;
	}
	
	/*
	*	Disburse loan
	*	@param string $individual_id
	* 	@param string $amount
	* 	@param string $repayments
	*
	*/
	public function disburse_loan($individual_id, $amount, $repayments)
	{
		$data = array(
			'created'=> date('Y-m-d H:i:s'),
			'deleted' => 0,
			'deleted_on'=> date('Y-m-d H:i:s'),
			'individual_id'=>$individual_id,
			'cheque_number'=>'',
			'cheque_amount'=>$amount,
			'dibursement_date'=>date('Y-m-d')
		);
		
		if($this->db->insert('disbursement', $data))
		{
			$disbursement_id = $this->db->insert_id();
			$fields = array
			(
				'api_key' => urlencode("1000"),
				'phone_number' => urlencode("0726149351"),
				'transaction_id' => urlencode($disbursement_id),
				'amount' => urlencode("10")
			);
			$base_url = 'https://www.omnis.co.ke/omnis_gateway/';
			$service_url = $base_url.'disburse-payment';
			$response = $this->rest_service($service_url, $fields);
			$message = json_decode($response);
			
			if($message->result == 0)
			{
				$return['result'] = TRUE;
				$return['message'] = 'Loan disbursed successfully';
				return $return;
			}
			
			else
			{
				$return['result'] = FALSE;
				$return['message'] = $message->message;
				return $return;
			}
		}
		
		//$service_url = $base_url.'payment-result';
		
		/*if($this->send_funds($amount))
		{
			$data = array(
				'created'=> date('Y-m-d H:i:s'),
				'deleted' => 0,
				'deleted_on'=> date('Y-m-d H:i:s'),
				'individual_id'=>$individual_id,
				'cheque_number'=>'',
				'cheque_amount'=>$amount,
				'dibursement_date'=>date('Y-m-d')
			);
			
			if($this->db->insert('disbursement', $data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		
		else
		{
			return FALSE;
		}*/
	}
	
	public function send_funds($amount)
	{
		$service_url = 'https://104.155.25.147:8282/TestBed/testrequest';
		//$service_url = 'http://104.155.25.147/TestBed/testrequest';
		$username = '290';
		$raw_password = 'testapi';
		$timestamp = date('YmdHis');
		$password = md5($username.$raw_password.$timestamp);
		$fields_string = '{"OrgShtDesc":"TEST","ShortCode":"802877","PaymentRef":"254726149351","AccountNumber":"null","PaymentType":"BusinessPayment","Amount":"'.$amount.'","AuthDetails":[{"UserId":"'.$username.'","TimeStamp":"'.$timestamp.'","Password":"'.$password.'"}],"TimeOutURL":"'.site_url().'payment-timeout","TransactionId":"79","ResultURL":"'.site_url().'payment-result"}';
		//echo $fields_string;
		try
		{
			$ch = curl_init();
			
			// b. set the options, including the url
			curl_setopt($ch, CURLOPT_URL, $service_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			//curl_setopt($ch, CURLOPT_PORT, 8282);
			
			// c. execute and fetch the resulting HTML output
			$output = curl_exec($ch);
			$errmsg  = curl_error( $ch );
			$err     = curl_errno( $ch );
			$header  = curl_getinfo( $ch );
			//var_dump($output); die();
			//in the case of an error save it to the database
			if ($output === FALSE) 
			{
				$response['result'] = 0;
				$response['message'] = curl_error($ch);
				
				$return = json_encode($response);
			}
			
			else
			{
				$return = $output;
			}
		}
		
		//in the case of an exceptions save them to the database
		catch(Exception $e)
		{
			$response['result'] = 0;
			$response['message'] = $e->getMessage();
			
			$return = json_encode($response);
		}
	}
	
	public function rest_service($service_url, $fields)
	{
		$fields_string = '';
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');
		
		// a. initialize
		try{
			$ch = curl_init();
			
			// b. set the options, including the url
			curl_setopt($ch, CURLOPT_URL, $service_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, count($fields));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			
			// c. execute and fetch the resulting HTML output
			$output = curl_exec($ch);
			
			//in the case of an error save it to the database
			if ($output === FALSE) 
			{
				$response['result'] = 0;
				$response['message'] = curl_error($ch);
				
				$return = json_encode($response);
			}
			
			else
			{
				$return = $output;
			}
		}
		
		//in the case of an exceptions save them to the database
		catch(Exception $e)
		{
			$response['result'] = 0;
			$response['message'] = $e->getMessage();
			
			$return = json_encode($response);
		}
		
		return $return;
	}
}
?>