<?php
//var_dump($loan_payment_details);

$disbursement_details = $loan_disbursement_details->row();

$cheque_number = $disbursement_details->cheque_number;
$cheque_amount = $disbursement_details->cheque_amount;
$dibursement_date = $disbursement_details->dibursement_date;
//var_dump($payment_amount. '-'.$payment_interest.'-' .$payment_date);
//repopulate data if validation errors occur
$validation_error = validation_errors();
if(!empty($validation_error))
{
	$cheque_number = set_value('cheque_number');
	$cheque_amount = set_value('cheque_amount');
	$dibursement_date = set_value('dibursement_date');
}
$error = $this->session->userdata('error_message');

if(!empty($error))
{
	echo '<div class="alert alert-danger">'.$error.'</div>';
	$this->session->unset_userdata('error_message');
}

$success = $this->session->userdata('success_message');

if(!empty($success))
{
	echo '<div class="alert alert-danger">'.$success.'</div>';
	$this->session->unset_userdata('success_message');
}
?>

<div class ="row" style="center-align">
	<div class = "col-md-12">
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Edit Disbursement</h2>
			</header>
			<div class="panel-body">
            
				<?php echo form_open('microfinance/loan-edit-loan-disbursement/'.$individual_id.'/'.$disbursement_id, array("class" => "form-horizontal", "role" => "form"));?>
					
				<div class="form-group">
            <label class="col-lg-5 control-label">Cheque Number: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="cheque_number" placeholder="Cheque Number" value="<?php echo $cheque_number;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Cheque Amount: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="cheque_amount" placeholder="Cheque Amount" value="<?php echo $cheque_amount;?>">
            </div>
        </div>
    	<div class="form-group">
            <label class="col-lg-5 control-label">Disbursement Date: </label>
            
            <div class="col-lg-7">
            	<div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="dibursement_date" placeholder="Disbursement Date" value="<?php echo $dibursement_date;?>">
                </div>
            </div>
        </div>
		<div class="form-actions center-align">
			<button class="btn btn-primary" type="submit">
				Edit disbursement
			</button>
		</div>

		<?php 
			echo form_close();
		?>
	</div>
</section>
</div>