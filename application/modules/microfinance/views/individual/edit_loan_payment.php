<?php
//var_dump($loan_payment_details);

$payment_details = $loan_payment_details->row();

$payment_amount = $payment_details->payment_amount;
$payment_interest = $payment_details->payment_interest;
$payment_date = $payment_details->payment_date;
//var_dump($payment_amount. '-'.$payment_interest.'-' .$payment_date);
//repopulate data if validation errors occur
$validation_error = validation_errors();
if(!empty($validation_error))
{
	$payment_amount = set_value('payment_amount');
	$payment_interest = set_value('payment_interest');
	$payment_date = set_value('payment_date');
}
$error = $this->session->userdata('error_message');

if(!empty($error))
{
	echo '<div class="alert alert-danger">'.$error.'</div>';
	$this->session->unset_userdata('error_message');
}

$success = $this->session->userdata('success_message');

if(!empty($success))
{
	echo '<div class="alert alert-danger">'.$success.'</div>';
	$this->session->unset_userdata('success_message');
}
?>

<div class ="row" style="center-align">
	<div class = "col-md-12">
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Edit payments</h2>
			</header>
			<div class="panel-body">
            
				<?php echo form_open('microfinance/edit-loan-payment/'.$individual_id.'/'.$loan_payment_id, array("class" => "form-horizontal", "role" => "form"));?>
					
				<div class="form-group">
					<label class="col-lg-5 control-label">Payment amount: </label>
					
					<div class="col-lg-7">
						<input type="text" class="form-control" value="<?php echo $payment_amount;?>" name="payment_amount" placeholder="Payment amount">
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-5 control-label">Interest amount: </label>
					
					<div class="col-lg-7">
						<input type="text" class="form-control" value="<?php echo $payment_interest;?>" name="payment_interest" placeholder="Interest amount">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-lg-5 control-label">Payment date: </label>
					
					<div class="col-lg-7">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date" value="<?php echo $payment_date;?>" placeholder="Payment date">
						</div>
					</div>
				</div>
				<div class="form-actions center-align">
					<button class="btn btn-primary" type="submit">
						Edit payment
					</button>
				</div>

				<?php 
					echo form_close();
				?>
			</div>
		</section>
    </div>