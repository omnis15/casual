<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once "./application/modules/auth/controllers/auth.php";
class Sms  extends MX_Controller
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('sms_model');
	}
	
	public function send_sms()

	{
		// Variables to Be Sent By other Applications
		$this->form_validation->set_rules('phone_number', 'Phone Number', 'required|xss_clean');
		//Second Field is how I want it to appear incase of Error
		//xxs clean is for security
		$this->form_validation->set_rules('message', 'message', 'required|xss_clean');
		$this->form_validation->set_rules('api_key', 'Api Key', 'required|xss_clean');
		$this->form_validation->set_rules('sender_id', 'Sender id', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$api_key = $this->input->post("api_key");

			if($this->sms_model->validateApi_Key($api_key)){
			$sender_id = $this->input->post("sender_id");
			$message = $this->input->post("message");
			$phone_number = $this->input->post("phone_number");
             /// This means go to SMS model find function Sms
			$result = $this->sms_model->sms($message, $api_key,$phone_number,$sender_id);

			}else{
                $response['result'] = FALSE;
				$response['message'] = "API Key Not Found...";
			}
			
	

		}
		
		else
		{
		 
		        // Validation Error gives errors based on validation rules
			    $response['result'] = FALSE;
				$response['message'] = validation_errors();
		}
		
	}
	public function payment_timeout()
	{
	}
	
	public function payment_result()
	{
	}
}
?>