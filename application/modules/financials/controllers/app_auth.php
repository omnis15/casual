<?php
class App_auth extends MX_Controller 
{
	function __construct()
	{
		parent:: __construct();
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		$this->load->model('auth/auth_model');
		$this->load->model('query_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/admin_model');
		$this->load->model('microfinance/payments_model');
		
	}
	
	public function index()
	{
		
	}
    
	
	public function sms_auth($phone, $amount,$task_cost_id)
	{
		
		$phone_sent = $this->query_model->clean_phone_number($phone);
		
		if($amount > 0)
		{
           
			$confirm_phone_number = $this->query_model->confirm_phone_number($phone);

			$fields_ser = array
						(
							'api_key' => urlencode("1000"),
							'phone_number' => urlencode($phone_sent),
							'transaction_id' => urlencode($task_cost_id),
							'amount' => urlencode($amount)
						);
			$base_url = 'https://www.omnis.co.ke/omnis_gateway/';
			$service_url = $base_url.'disburse-payment';
			$response2 = $this->rest_service($service_url, $fields_ser);
			$message = json_decode($response2);
			$message = json_decode($message);
						
		
		}
		else
		{
			$response['result'] = 0;
			$response['message'] = 'The amount provided is wrong...';
		}
		
		
	}
	
	
	
}
?>