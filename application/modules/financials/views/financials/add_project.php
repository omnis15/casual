<?php
//personnel data
$project_title = set_value('project_title');
$project_financier = set_value('project_financier');
$project_description = set_value('project_description');
$project_county = set_value('project_county');
$project_value = set_value('project_value');

?>   
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>projects" class="btn btn-info pull-right">Back to Projects</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                        $success = $this->session->userdata('success_message');
                        $error = $this->session->userdata('error_message');
                        
                        if(!empty($success))
                        {
                            echo '
                                <div class="alert alert-success">'.$success.'</div>
                            ';
                            
                            $this->session->unset_userdata('success_message');
                        }
                        
                        if(!empty($error))
                        {
                            echo '
                                <div class="alert alert-danger">'.$error.'</div>
                            ';
                            
                            $this->session->unset_userdata('error_message');
                        }
                        $validation_errors = validation_errors();
                        
                        if(!empty($validation_errors))
                        {
                            echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                        }
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
       
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Project Title: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="project_title" placeholder="Project Title" value="<?php echo $project_title;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Project Financier: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="project_financier" placeholder="Project Financier" value="<?php echo $project_financier;?>">
            </div>
        </div>
         <div class="form-group">
            <label class="col-lg-5 control-label">Project Goal: </label>
            
            <div class="col-lg-7">
                 <textarea rows="4" cols="35" name="project_description" value="<?php echo $project_description;?>">
                 </textarea> 
            </div>
        </div>
         
        
    </div>
    
    <div class="col-md-6">
        
       
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Project Value: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="project_value" placeholder="kes." value="<?php echo $project_value;?>">
            </div>
        </div>
        
        <div class="form-group">
             <label class="col-lg-5 control-label">Project County: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="project_county">
                    <option value="">--Select County--</option>
                    <?php
                        if($counties->num_rows() > 0)
                        {
                            $veh = $counties->result();
                            
                            foreach($veh as $res)
                            {
                                $county_id = $res->county_id;
                                $county_name = $res->county_name;
                                
                              
                                echo '<option value="'.$county_id.'">'.$county_name.'</option>';
                                
                            }
                        }
                    ?>
                </select>
             </div>
        </div>

        
        
       
        

    </div>

    

</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add Project
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>