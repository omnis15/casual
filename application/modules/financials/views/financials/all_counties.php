<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>County Name</th>
						<th>Created</th>
						<th>Status</th>
						<th colspan="1">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$project_id = $row->county_id;
				$project_title = $row->county_name;
				$project_status = $row->county_status;
				
				$created = $row->last_modified;

				
				
				//status
				if($project_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				//status
				
				//create deactivated status display
				if($project_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-danger" href="'.site_url().'projects/delete-county/'.$project_id.'" onclick="return confirm(\'Do you want to Delete '.$project_title.'?\');" title="Delete '.$project_title.'"> Delete</a>';
					

				
				} 
				//create activated status display
				else if($project_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-danger" href="'.site_url().'project-administration/counties-delete/'.$project_id.'" onclick="return confirm(\'Do you want to Delete '.$project_title.'?\');" title="Delete '.$project_title.'"> Delete</a>';
					
				
				}
				
				
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$project_title.'</td>
						<td>'.$created.'</td>
						<td>'.$status.'</td>			
						<td>'.$button.'</td>
						
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no Counties";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				<div class="row" style="margin-bottom:20px;">
                                    <div class="col-lg-2 col-lg-offset-10">
                                        <a href="<?php echo site_url();?>project-administration/counties-add" class="btn btn-sm btn-info pull-right">Add Counties</a>
                                     </div>
                                    
                                   
                                </div>
				

				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

