<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Casual Name</th>
						<th>Phone</th>
						<th>National ID</th>
						<th>Gender</th>
						<th>Amount</th>
						<th>Status</th>
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$casual_id = $row->casual_id;
				$task_casual_id = $row->task_casual_id;
				$casual_name = $row->casual_name;
				$casual_phone = $row->casual_phone;
				$casual_national_id = $row->casual_national_id;
				$casual_gender = $row->casual_gender;
				if($casual_gender == 1){
					$gender = "Male";
				}
				else{
					$gender = "Female";

				}
				$amount = $row->amount;
				$project_id = $row->project_id;
				$project_status = $row->payment_status;
				$payment_date = $row->payment_date;
				

				
				

				
				
				//status
				if($project_status == 1)
				{
					$status = 'Casual Paid';
				}
				else
				{
					$status = 'Casual Not Paid';
				}
				//status
				
				//create deactivated status display
				if($project_status == 1)
				{
					$status = '<span class="label label-success">Casual Paid</span>';
					$button = '';
					$button_edit='Payment Date: '.$payment_date;
					$button_tasks = '<button type="button" class="btn  btn-warning" data-toggle="modal" data-target="#book-appointment'.$casual_id.'"><i class="fa fa-inbox"></i> Send SMS </button>
									<div class="modal fade " id="book-appointment'.$casual_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									    <div class="modal-dialog modal-lg" role="document">
									        <div class="modal-content ">
									            <div class="modal-header">
									            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									            	<h4 class="modal-title" id="myModalLabel">Send a Message to: '.$casual_name.'</h4>
									            </div>
									            '.form_open("financials/submit_casual_message/".$casual_id.'/'.$task_cost_id, array("class" => "form-horizontal")).'

									            <div class="modal-body">
									            	<div class="row">
									            		<input type="hidden" name="redirect_url" id="redirect_url'.$casual_id.'" value="'.$this->uri->uri_string().'">
									            		<input type="text" name="phone'.$casual_id.'" id="phone'.$casual_id.'" value="'.$casual_phone.'">
									            		<div class="row" style="margin-bottom:10px;">
									            			<div class="col-md-10">
																 <div class="form-group">
												                        <label class="col-lg-4 control-label">Message</label>
												                        <div class="col-lg-8">
												                        	<textarea class="form-control" name="message'.$casual_id.'" id="message'.$casual_id.'"></textarea>
												                           
												                       </div>
									                             </div>  
									            			</div>
									            		</div>
									            	</div>
									            </div>
									            <div class="modal-footer">
									            	<button type="submit" class="btn btn-sm btn-success" >Submit Message</button>
									                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
									            </div>

									               '.form_close().'
									        </div>
									    </div>
									</div>
							';

				
				}
				//create activated status display
				else if($project_status == 0)
				{
					$status = '<span class="label label-warning">Casual Not Paid</span>';
					$button_edit = '<a class="btn btn-success" href="'.site_url().'financials/sms_auth/'.$casual_phone.'/'.$amount.'/'.$task_cost_id.'/'.$task_casual_id.'"><i class="fa fa-money"></i> M-PESA Kes.'.$amount.'</a>';
			        $button_tasks = '<button type="button" class="btn  btn-warning" data-toggle="modal" data-target="#book-appointment'.$casual_id.'"><i class="fa fa-inbox"></i> Send SMS </button>
									<div class="modal fade " id="book-appointment'.$casual_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									    <div class="modal-dialog modal-lg" role="document">
									        <div class="modal-content ">
									            <div class="modal-header">
									            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									            	<h4 class="modal-title" id="myModalLabel">Send a Message to: '.$casual_name.'</h4>
									            </div>
									            '.form_open("financials/submit_casual_message/".$casual_id.'/'.$task_cost_id, array("class" => "form-horizontal")).'

									            <div class="modal-body">
									            	<div class="row">
									            		<input type="hidden" name="redirect_url" id="redirect_url'.$casual_id.'" value="'.$this->uri->uri_string().'">
									            		<input type="text" name="phone'.$casual_id.'" id="phone'.$casual_id.'" value="'.$casual_phone.'">
									            		<div class="row" style="margin-bottom:10px;">
									            			<div class="col-md-10">
																 <div class="form-group">
												                        <label class="col-lg-4 control-label">Message</label>
												                        <div class="col-lg-8">
												                        	<textarea class="form-control" name="message'.$casual_id.'" id="message'.$casual_id.'"></textarea>
												                           
												                       </div>
									                             </div>  
									            			</div>
									            		</div>
									            	</div>
									            </div>
									            <div class="modal-footer">
									            	<button type="submit" class="btn btn-sm btn-success" >Submit Message</button>
									                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
									            </div>

									               '.form_close().'
									        </div>
									    </div>
									</div>
							';
			        
				}
				
				
				
				
				$count++;
				$result .= 
				'
					<tr>
					
						<td>'.$count.'</td>
					    <td>'.$casual_name.'</td>
					    <td>'.$casual_phone.'</td>
					    <td>'.$casual_national_id.'</td>
					    <td>'.$gender.'</td>
					    <td>'.$amount.'</td>
						<td>'.$status.'</td>			
						<td>'.$button_tasks.'</td>
						<td>'.$button_edit.'</td>
						
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no Casuals";
		}
?>


<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                 <a href="<?php echo site_url();?>" class="btn btn-sm btn-success pull-right" data-toggle="modal" data-target="#send-bulk"><i class="fa fa-inbox"></i> Send BulK Money</a>
                <button type="button" class="btn btn-sm btn-warning pull-right" data-toggle="modal" data-target="#send-bulk"><i class="fa fa-inbox"></i> Send BulK Sms </button>

                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				
				<div class="modal fade " id="send-bulk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				    <div class="modal-dialog modal-lg" role="document">
				        <div class="modal-content ">
				            <div class="modal-header">
				            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				            	<h4 class="modal-title" id="myModalLabel">Send Bulk Messages</h4>
				            </div>
				            <?php echo form_open("financials/submit_bulk_messages/".$task_cost_id, array("class" => "form-horizontal"));?>

				            <div class="modal-body">
				            	<div class="row">
				            		<div class="row" style="margin-bottom:10px;">
				            			<div class="col-md-10">
				            				<input type="hidden" name="redirect_url" id="redirect_url'.$casual_id.'" value="<?php echo $this->uri->uri_string();?>">
											 <div class="form-group">
							                        <label class="col-lg-4 control-label">Message</label>
							                        <div class="col-lg-8">
							                        	<textarea class="form-control" name="message"></textarea>
							                           
							                       </div>
				                             </div>  
				            			</div>
				            		</div>
				            	</div>
				            </div>
				            <div class="modal-footer">
				            	<button type="submit" class="btn btn-sm btn-success" >Submit Message</button>
				                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
				            </div>

				               <?php echo form_close();?>
				        </div>
				    </div>
				</div>

				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

