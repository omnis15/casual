
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>customers" class="btn btn-info pull-right">Back to Customers</a>
                        </div>
                    </div>
                    <!-- Adding Errors -->
            <?php
            if(isset($error)){
                echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
            }
            
			//the vehicle details
			$customer_contacts_id = $customer_contacts[0]->customer_contacts_id;
			$customer_contacts_first_name = $customer_contacts[0]->customer_contacts_first_name;
			$customer_contacts_sur_name = $customer_contacts[0]->customer_contacts_sur_name;
			$customer_contacts_phone = $customer_contacts[0]->customer_contacts_phone;
			$customer_contacts_email = $customer_contacts[0]->customer_contacts_email;
			
            
            $validation_errors = validation_errors();
            
            if(!empty($validation_errors))
            {
				$customer_contacts_first_name = set_value('customer_contacts_first_name');
				$customer_contacts_sur_name = set_value('customer_contacts_sur_name');
				$customer_contacts_phone = set_value('customer_contacts_phone');
				$customer_contacts_email = set_value('customer_contacts_email');
		
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
			
            ?>
            
          <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
       
        
         <div class="form-group">

            <label class="col-lg-5 control-label">First Name: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="customer_contacts_first_name" placeholder="First Name" value="<?php echo $customer_contacts_first_name;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">SurName: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="customer_contacts_sur_name" placeholder="Surname" value="<?php echo $customer_contacts_sur_name;?>">
            </div>
        </div>
        
        
    </div>
    
    <div class="col-md-6">
        
       
        
       <div class="form-group">
            <label class="col-lg-5 control-label">Phone Number: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="customer_contacts_phone" placeholder="Phone Number" value="<?php echo $customer_contacts_phone;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Email Address: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="customer_contacts_email" placeholder="Email Address" value="<?php echo $customer_contacts_email;?>">
            </div>
        </div>
        
    

    </div>
</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Edit Company Contact
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>