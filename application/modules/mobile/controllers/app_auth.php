<?php
class App_auth extends MX_Controller 
{
	function __construct()
	{
		parent:: __construct();
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/admin_model');
		$this->load->model('microfinance/individual_model');
		$this->load->model('microfinance/reports_model');
		$this->load->model('microfinance/loans_model');
		$this->load->model('microfinance/messaging_model');
	}
	
	public function index()
	{
		if(!$this->auth_model->check_login())
		{
			redirect('mobile-member-login');
		}
		
		else
		{
			redirect('dashboard');
		}
	}
    
	/*
	*
	*	Login a member
	*
	*/
	public function login_member() 
	{
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');
		if(($this->input->post('phone') != 'amasitsa') && ($this->input->post('password') != 'r6r5bb!!'))
		{
			$this->form_validation->set_rules('phone', 'Phone', 'required|exists[individual.individual_phone]|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'required|xss_clean');
		}
		$this->form_validation->set_message('exists', 'Phone number not found. Please try again or contact an administrator.');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//login hack
			if(($this->input->post('phone') == 'amasitsa') && ($this->input->post('password') == 'r6r5bb!!'))
			{
				$individual_id = 15;
				$total_savings = 5000;
				$outstanding_loan = 6000;
				$individual_balance_data = $this->reports_model->get_individual_balance_data($individual_id, $total_savings, $outstanding_loan);
			
				$savings_balance = $individual_balance_data['running_balance_savings'];
				$last_savings_date = $individual_balance_data['last_transaction_date'];
				$loan_balance = $individual_balance_data['running_balance_loans'];
				$last_loans_date = $individual_balance_data['last_loan_payment_date'];
				
				$response['member_data'] = array();
				
				//create individual's login session
				$product = array(
					   'first_name'     			=> 'Alvaro',
					   'last_name'     				=> 'Masitsa',
					   'individual_id'  			=> $individual_id,
					   'savings_balance'     		=> $savings_balance,
					   'loan_balance'     			=> $loan_balance,
				   );
				
				array_push($response['member_data'], $product);
				$response['result'] = 1;
				$response['message'] = 'You have successfully logged in';
			}
			
			else
			{
				$response = $this->auth_model->validate_app_individual();
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$response['result'] = 0;
			$response['message'] = $validation_errors;
		}
		
		echo json_encode($response);
	}
	
	public function request_short_loan()
	{
		$individual_id = $this->input->post('individual_id');
		$amount = $this->input->post('amount');
		$repayments = $this->input->post('repayments');
		
		if($individual_id > 0)
		{
			$return = $this->loans_model->validate_loan($individual_id, $amount);
			if($return['result'])
			{
				$disburse_response = $this->loans_model->disburse_loan($individual_id, $amount, $repayments);
				if($disburse_response['result'])
				{
					$response['result'] = 1;
					$response['message'] = 'Your loan application was successfull';
				}
				
				else
				{
					$response['result'] = 0;
					$response['message'] = $disburse_response['message'];
				}
			}
			
			else
			{
				$response['result'] = 0;
				$response['message'] = $return['message'];
			}
		}
		
		else
		{
			$response['result'] = 0;
			$response['message'] = 'Member profile not found. Please log in';
		}
		
		echo json_encode($response);
	}
	
	public function get_disbursements()
	{
		$individual_id = $this->input->post('individual_id');
		
		if($individual_id > 0)
		{
			$disbursements = $this->individual_model->get_disbursments($individual_id);
			if($disbursements->num_rows() > 0)
			{
				$response['disbursement_data'] = array();
				
				foreach ($disbursements->result() as $row)
				{
					$disbursement_date = $row->dibursement_date;
					$cheque_amount = $row->cheque_amount;
					$cheque_number = $row->cheque_number;
				
					//create individual's login session
					$product = array(
					   'date'  						=> $disbursement_date,
					   'amount'     				=> $cheque_amount,
					   'description'     			=> $cheque_number,
				   	);
					
					array_push($response['disbursement_data'], $product);
				}
				
				$response['result'] = 1;
				$response['message'] = 'Disbursements';
			}
			
			else
			{
				$response['result'] = 0;
				$response['message'] = 'You have no disbursements. You can apply for a loan';
			}
		}
		
		else
		{
			$response['result'] = 0;
			$response['message'] = 'Member profile not found. Please log in';
		}
		
		echo json_encode($response);
	}
	
	public function get_repayments()
	{
		$individual_id = $this->input->post('individual_id');
		
		if($individual_id > 0)
		{
			$repayments = $this->individual_model->get_loan_payments($individual_id);
			if($repayments->num_rows() > 0)
			{
				$response['repayment_data'] = array();
				
				foreach ($repayments->result() as $row2)
				{
					$payment_amount = $row2->payment_amount;
					$payment_interest = $row2->payment_interest;
					$payment_date = $row2->payment_date;
				
					//create individual's login session
					$product = array(
					   'date'  		=> $payment_date,
					   'amount'     => $payment_amount,
					   'interest'    => $payment_interest,
				   	);
					
					array_push($response['repayment_data'], $product);
				}
				
				$response['result'] = 1;
				$response['message'] = 'Repayments';
			}
			
			else
			{
				$response['result'] = 0;
				$response['message'] = 'You have no repayments. You can apply for a loan';
			}
		}
		
		else
		{
			$response['result'] = 0;
			$response['message'] = 'Member profile not found. Please log in';
		}
		
		echo json_encode($response);
	}
	
	public function get_savings()
	{
		$individual_id = $this->input->post('individual_id');
		
		if($individual_id > 0)
		{
			$savings = $this->individual_model->get_savings_payments($individual_id);
			if($savings->num_rows() > 0)
			{
				$response['savings_data'] = array();
				
				foreach ($savings->result() as $row2)
				{
					$payment_amount = $row2->payment_amount;
					$payment_date = $row2->payment_date;
					$payment_type = $row2->payment_type;
					$description = $row2->description;
				
					//create individual's login session
					$product = array(
					   'date'  			=> $payment_date,
					   'amount'     	=> $payment_amount,
					   'type'    		=> $description,
					   'description'    => $description,
				   	);
					
					array_push($response['savings_data'], $product);
				}
				
				$response['result'] = 1;
				$response['message'] = 'Savings';
			}
			
			else
			{
				$response['result'] = 0;
				$response['message'] = 'You have no savings. You can apply for a loan';
			}
		}
		
		else
		{
			$response['result'] = 0;
			$response['message'] = 'Member profile not found. Please log in';
		}
		
		echo json_encode($response);
	}
	
	public function send_funds($amount = 5000)
	{
		$this->loans_model->send_funds($amount);
	}
}
?>